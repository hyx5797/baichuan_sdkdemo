//
//  AppDelegate.m
//  百川SDKDemo
//
//  Created by 何乾坤 on 2020/2/22.
//  Copyright © 2020 何乾坤. All rights reserved.
//

#import "AppDelegate.h"
#import <AlibcTradeSDK/AlibcTradeSDK.h>
#import <AlibabaAuthSDK/albbsdk.h>
//#import "CeViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize window = _window;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    // 百川平台基础SDK初始化，加载并初始化各个业务能力插件
        ALBBSDK *albbSDK = [ALBBSDK sharedInstance];
        [[AlibcTradeSDK sharedInstance] setDebugLogOpen:YES];
        [[AlibcTradeSDK sharedInstance] setIsvAppName:@"600"];
        [[AlibcTradeSDK sharedInstance] setIsvVersion:@"2.2.2"];
        [albbSDK setAppkey:@"28414632"];
        [albbSDK setAuthOption:NormalAuth];
    //    [[AlibcTradeSDK sharedInstance] setEnv:AlibcEnvironmentRelease];
        [[AlibcTradeSDK sharedInstance]asyncInitWithSuccess:^{
            TLOG_INFO(@"百川SDK初始化成功");
        } failure:^(NSError *error) {
            TLOG_INFO(@"百川SDK初始化失败");
        }];
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    // 如果百川处理过会返回YES
    if (![[AlibcTradeSDK sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation]) {
        // 处理其他app跳转到自己的app
    }
    return YES;
}


//IOS9.0 系统新的处理openURL 的API
//-(B00L )application : (UIApplication *) application openURL : (NSURL *)url options : (NSDictionary<NSString *,id> *)options (
//11 irt8ẞ5i
//if (![[AlibcTradeSDK sharedInstance] application: application
//openURL :url
//options options]) f
//11 xbHHftappikstEJÉ ENJapp, XQR EIIklit iE®YES
//H
//return YES;
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options {
    
    if (@available(iOS 9.0, *)) {
        if(![[AlibcTradeSDK sharedInstance] application:application openURL:url options:options]){
            
        }
    } else {
        // Fallback on earlier versions
    }
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
